import UIKit
import Foundation

//Task-1


func sortByNameSurName(first: Student, second: Student) -> Bool{
    if first.surname != second.surname{
        return (first.surname < second.surname)
    }
    else{
        return (first.name < second.name)
    }
}

func sortByNameSurName(first: StudentClass, second: StudentClass) -> Bool{
    if first.surname != second.surname{
        return (first.surname < second.surname)
    }
    else{
        return (first.name < second.name)
    }
}
//Создаем Структуру для даты рождения

struct DateBirth{
    let day : UInt8
    let month : UInt8
    let year : Int
    init(day : UInt8, month : UInt8, year : Int){
        self.day = day
        self.month = month
        self.year = year
    }
}

//Создаем класс для даты рождения

class DateBirthClass{
    let day : UInt8
    let month : UInt8
    let year : Int
    init(day : UInt8, month : UInt8, year : Int){
        self.day = day
        self.month = month
        self.year = year
    }
}


//Создаем структуру для студента
struct Student{
    var name : String
    var surname : String
    var avarangeRating : Double
    var dateBirth : DateBirth
    
    init(name: String, surname : String, avarangeRating : Double, dateBirth : DateBirth){
        self.name = name
        self.surname = surname
        self.avarangeRating = avarangeRating
        self.dateBirth = dateBirth
    }
}


//Создаем класс для студента
class StudentClass{
    var name : String
    var surname : String
    var avarangeRating : Double
    var dateBirth : DateBirth
    
    init(name: String, surname : String, avarangeRating : Double, dateBirth : DateBirth){
        self.name = name
        self.surname = surname
        self.avarangeRating = avarangeRating
        self.dateBirth = dateBirth
    }
}


//Создаем экземпляры структур
var student1Class = StudentClass(name: "Paul", surname: "Chvyrov", avarangeRating: 4.35, dateBirth: DateBirth(day: 12, month: 10, year: 1998))

var student2Class = StudentClass(name: "Ilya", surname: "Lomaev", avarangeRating: 4.6, dateBirth: DateBirth (day: 20, month: 9, year: 2004))

var student3Class = StudentClass(name: "Den", surname: "Derendyaev", avarangeRating: 5, dateBirth: DateBirth(day: 4, month: 9, year: 1999))

var student4Class = StudentClass(name: "Paul", surname: "Lomaev", avarangeRating: 4.6, dateBirth: DateBirth (day: 20, month: 9, year: 2004))






//Создаем экземпляры класса
var student1 = Student(name: "Paul", surname: "Chvyrov", avarangeRating: 4.35, dateBirth: DateBirth(day: 12, month: 10, year: 1998))

var student2 = Student(name: "Ilya", surname: "Lomaev", avarangeRating: 4.6, dateBirth: DateBirth (day: 20, month: 9, year: 2004))

var student3 = Student(name: "Den", surname: "Derendyaev", avarangeRating: 5, dateBirth: DateBirth(day: 4, month: 9, year: 1999))

var student4 = Student(name: "Paul", surname: "Lomaev", avarangeRating: 4.6, dateBirth: DateBirth (day: 20, month: 9, year: 2004))





//Task-2
var gradeBook = [student1, student2, student3, student4]
var gradeBookClass = [student1Class, student2Class, student3Class, student4Class]
//Функция для печати журнала
func printGradeBook(arrayIn : [Student]){
    print("№ |Фамилия     |Имя         |Средний балл|Дата рождения")
    var j = 1
    for i in arrayIn{
        print(j, terminator: ")|")
        print(i.surname, terminator: "")
        
        var tempTerminator = 12 - i.surname.count
        for _ in 0..<tempTerminator{
            print("", terminator: " ")
        }
        
        tempTerminator = 12 - i.name.count
        print("", terminator: "|")
        print(i.name, terminator: "")
        for _ in 0..<tempTerminator{
            print("", terminator: " ")
        }
        
        print("", terminator: "|")
        print(NSString(format: "%.2f", i.avarangeRating), terminator: "")
        for _ in 0..<8{
            print("", terminator: " ")
        }
        
        print("", terminator: "|")
        
        if i.dateBirth.day < 10{
            print("0", terminator: "")
        }
        print("\(i.dateBirth.day).", terminator: "")
        
        if i.dateBirth.month < 10{
            print("0", terminator: "")
        }
        print("\(i.dateBirth.month).", terminator: "")
        
        print(i.dateBirth.year)
        j += 1
    }
    print()
}

func printGradeBookClass(arrayIn : [StudentClass]){
    print("№ |Фамилия     |Имя         |Средний балл|Дата рождения")
    var j = 1
    for i in arrayIn{
        print(j, terminator: ")|")
        print(i.surname, terminator: "")
        
        var tempTerminator = 12 - i.surname.count
        for _ in 0..<tempTerminator{
            print("", terminator: " ")
        }
        
        tempTerminator = 12 - i.name.count
        print("", terminator: "|")
        print(i.name, terminator: "")
        for _ in 0..<tempTerminator{
            print("", terminator: " ")
        }
        
        print("", terminator: "|")
        print(NSString(format: "%.2f", i.avarangeRating), terminator: "")
        for _ in 0..<8{
            print("", terminator: " ")
        }
        
        print("", terminator: "|")
        
        if i.dateBirth.day < 10{
            print("0", terminator: "")
        }
        print("\(i.dateBirth.day).", terminator: "")
        
        if i.dateBirth.month < 10{
            print("0", terminator: "")
        }
        print("\(i.dateBirth.month).", terminator: "")
        
        print(i.dateBirth.year)
        j += 1
    }
    print()
}

print("Структура")
printGradeBook(arrayIn: gradeBook)

print("Класс")
printGradeBookClass(arrayIn: gradeBookClass)






//Task-3
print("Сортировка по среднему баллу")

print("Структура")
gradeBook.sort {$0.avarangeRating > $1.avarangeRating}
printGradeBook(arrayIn: gradeBook)


print("Класс")
gradeBookClass.sort {$0.avarangeRating > $1.avarangeRating}
printGradeBookClass(arrayIn: gradeBookClass)





//Task-4

print("Сортировака по имени и фамилии")

print("Структура")
gradeBook.sort(by: sortByNameSurName(first:second:))
printGradeBook(arrayIn: gradeBook)


print("Класс")
gradeBookClass.sort(by: sortByNameSurName(first:second:))
printGradeBookClass(arrayIn: gradeBookClass)



//Task-5
print("Структура")


var gradeBookV2 = gradeBook
gradeBookV2[1].surname = "Petrov"
print("v2")
printGradeBook(arrayIn: gradeBookV2)
print("standart")
printGradeBook(arrayIn: gradeBook)



print("Класс")
var gradeBookClassV2 = gradeBookClass

if gradeBookClass[0] === gradeBookClassV2[0]{
    print(1)
}

gradeBookClass[1].surname = "Petrov"
print("v2")
printGradeBookClass(arrayIn: gradeBookClassV2)
print("standart")
printGradeBookClass(arrayIn: gradeBookClass)

